<?php
/**
 * @file
 * Views Galleriffic theme wrapper.
 *
 * @ingroup views_templates
 */

if($count_images == 1):
?>
<div style="width:800px;background:#eeeeee;margin:50px auto;border-radius:20px;" id="">
  <div style="padding:30px;">
      <div id="<?php print $view_name; ?>"style="height:100px;"></div>
  </div>
</div>
<?php endif
else:
  print "There are no images to be displayed. Please add some images into the field";
?>
