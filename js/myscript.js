/**
 * @file
 * Stores image urls in an array and pass them to onebook script.
 */

(function($) {
  Drupal.behaviors.imagebook = {
    attach: function (context, settings) {
      if (settings.imagebook !== undefined){
          var src5 = [];
          for (var k in settings.imagebook.testvar){
            if (settings.imagebook.testvar.hasOwnProperty(k)) {
                src5[k] = settings.imagebook.testvar[k];
            }
          }
          var src1 = [];
          for (var k in settings.imagebook.imagevar){
            if (settings.imagebook.imagevar.hasOwnProperty(k)) {
                src1[k] = settings.imagebook.imagevar[k];
            }
          }
      }
      $(document).ready(function() {
        if(src5.length != 0){
          $('#imagebook').onebook(src5,{skin:['light','dark'], bgDark:'#222222', flip:'soft', border:25, cesh:false});
        }
        if(settings.imagebook.myvar !== undefined){
          for (var i = 0; i < settings.imagebook.myvar.length; i++) {
            if(src1.length != 0){
              $("#" + settings.imagebook.myvar[i]).onebook(src1,{skin:['light','dark'], bgDark:'#222222', flip:'soft', border:25, cesh:false});
            }
          }
        }
      });
    }
  };
})(jQuery);
