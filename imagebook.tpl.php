<?php

/**
 * @file
 * Display the image field as a book using plugin.
 */

if($count_img == 0): ?>
<div style="width:800px;background:#eeeeee;margin:50px auto;border-radius:20px;">
	<div style="padding:30px;">
		<div id="imagebook"></div>
	</div>
</div>
<?php
else :
  print "There are no images to be displayed in imagebook. Please add some images into the field.";
endif;?>

