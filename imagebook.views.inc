<?php

/**
 * @file
 * Provide the Galleriffic plugin definition.
 */

/**
 * Implements hook_views_plugins().
 */
function imagebook_views_plugins() {
  $path = drupal_get_path('module', 'imagebook');
  return array(
    'module' => 'imagebook',
    'style' => array(
      'imagebook' => array(
        'title' => t('Image Book'),
        'help' => t('Display a view like a Image Book.'),
        'handler' => 'Imagebookstyle',
        'theme' => 'imagebook_view',
        'theme path' => $path,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => TRUE,
        'type' => 'normal',
        'parent' => 'default',
      ),
    ),
    'row' => array(
      'imagebookfields' => array(
        'title' => t('Image Book'),
        'help' => t('Choose the fields to display in the Image Book.'),
        'handler' => 'Imagebookrows',
        'theme' => 'imagebook_view_imagebookrows',
        'theme path' => $path,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
