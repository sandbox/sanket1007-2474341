<?php
/**
 * @file
 * Contains the base row style plugin.
 */

/**
 * The basic 'fields' row plugin.
 *
 * This displays fields one after another, giving options for inline
 * or not.
 *
 * @ingroup views_row_plugins
 */
class Imagebookrows extends views_plugin_row {
  /**
   * Implements options_form().
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['image_field'] = array('default' => '');
    return $options;
  }

  /**
   * Implements options_form().
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Pre-build all of our option lists for the dials and switches that follow.
    $fields = array('' => t('<none>'));
    $form['image_field'] = array(
      '#type' => 'select',
      '#title' => t('Image field'),
      '#options' => $fields,
      '#default_value' => $this->options['image_field'],
      '#description' => t('Select the field that will be used as the image slide. REQUIRED.'),
    );
  }

}
